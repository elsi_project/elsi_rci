# ELectronic Structure Infrastructure Reverse Communication Interface
# ELSI-RCI (v0.1.0)

## About

ELSI-RCI provides and enhances open-source software packages
which iteratively solve or circumvent eigenvalue problems
in self-consistent field calculations based on the Kohn-Sham
density-functional theory. For more information, please visit the
[ELSI interchange](http://elsi-interchange.org) website.

## Installation

The standard installation of ELSI-RCI requires:

* CMake (3.0.2 or newer)
* Fortran compiler (Fortran 2003)
* BLAS, LAPACK

Installation with recent versions of GNU, and Intel compilers has
been tested. For a complete description of the installation process,
please refer to the documentation.

## More

A User's Guide is available at
[`./doc/elsi_rci_manual.pdf`](./doc/elsi_rci_manual.pdf).
For comments, feedback, and suggestions, please [contact the ELSI
team](mailto:elsi-team@duke.edu).

Copyright (c) 2015-2019, the ELSI team. All rights reserved.
