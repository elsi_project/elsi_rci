### Generic GNU ###

SET(CMAKE_Fortran_COMPILER "gfortran" CACHE STRING "Fortran compiler")
SET(CMAKE_Fortran_FLAGS "-O3" CACHE STRING "Fortran flags")

SET(LIB_PATHS "$ENV{MKLROOT}/lib/intel64" CACHE STRING "External library paths")
SET(LIBS "mkl_intel_lp64 mkl_sequential mkl_core" CACHE STRING "External libraries")
