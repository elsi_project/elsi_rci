! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module provides public-facting interface routines for using ELSI RCI.
!!
module ELSI_RCI

    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE

    use ELSI_RCI_SETUP

    use ELSI_RCI_DAVIDSON
    use ELSI_RCI_OMM
    use ELSI_RCI_PPCG
    use ELSI_RCI_CHEBFILTER

    implicit none

contains

    subroutine rci_solve_allocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        select case (r_h%solver)
        case (RCI_SOLVER, RCI_SOLVER_DAVIDSON)
            call rci_davidson_allocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_OMM)
            call rci_omm_allocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_PPCG)
            call rci_ppcg_allocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_CHEBFILTER)
            call rci_chebfilter_allocate(r_h, ijob, iS, task)
        end select

    end subroutine

    subroutine rci_solve_deallocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        select case (r_h%solver)
        case (RCI_SOLVER, RCI_SOLVER_DAVIDSON)
            call rci_davidson_deallocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_OMM)
            call rci_omm_deallocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_PPCG)
            call rci_ppcg_deallocate(r_h, ijob, iS, task)
        case (RCI_SOLVER_CHEBFILTER)
            call rci_chebfilter_deallocate(r_h, ijob, iS, task)
        end select

    end subroutine

    subroutine rci_solve(r_h, ijob, iS, task, resvec)

        implicit none

        !**** INPUT ***********************************!

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        type(rci_handle), intent(inout) :: r_h
        integer(i4), intent(inout) :: ijob ! job id
        real(r8), intent(inout) :: resvec(:)
        type(rci_instr), intent(inout) :: iS

        select case (r_h%solver)
        case (RCI_SOLVER, RCI_SOLVER_DAVIDSON)
            call rci_davidson(r_h, ijob, iS, task, resvec)
        case (RCI_SOLVER_OMM)
            call rci_omm(r_h, ijob, iS, task, resvec)
        case (RCI_SOLVER_PPCG)
            call rci_ppcg(r_h, ijob, iS, task, resvec)
        case (RCI_SOLVER_CHEBFILTER)
            call rci_chebfilter(r_h, ijob, iS, task, resvec)
        end select
    end subroutine

end module ELSI_RCI
