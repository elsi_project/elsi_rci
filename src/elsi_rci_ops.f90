! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains operations used within RCI.
!!
module ELSI_RCI_OPS

    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:r8, i4

    implicit none

contains

    ! Null
    ! - rci_op_null(task)

    ! Converge flag
    ! - rci_op_converge(task,conv_flag)

    ! Stop
    ! - rci_op_stop(task)

    ! Allocate
    ! - rci_op_allocate(iS,task,m,n,Aidx)

    ! Deallocate
    ! - rci_op_deallocate(iS,task,Aidx)

    ! B = H^(trH) * A
    ! - rci_op_h_multi(iS,task,trH,m,n,Aidx,Bidx)

    ! B = S^(trS) * A
    ! - rci_op_s_multi(iS,task,trS,m,n,Aidx,Bidx)

    ! B = P^(trP) * A
    ! - rci_op_p_multi(iS,task,trP,m,n,Aidx,Bidx)

    ! B = A^(trA)
    ! - rci_op_copy(iS,task,trA,Aidx,Bidx)

    ! B(rBoff+(1:m),cBoff+(1:n)) = A(rAoff+(1:m),cAoff+(1:n))
    ! - rci_op_subcopy(iS,task,m,n,Aidx,lda,rAoff,cAoff,Bidx,ldb,
    !                  rBoff,cBoff)

    ! B = A(:,logical(resvec))
    ! - rci_op_subcol(iS,task,m,n,Aidx,Bidx)

    ! B = A(logical(resvec),:)
    ! - rci_op_subrow(iS,task,m,n,Aidx,Bidx)

    ! resvec(1) = trace(A)
    ! - rci_op_trace(iS,task,n,Aidx,lda)

    ! resvec(1) = trace(A * B)
    ! - rci_op_dot(iS,task,m,n,Aidx,lda,Bidx,ldb)

    ! A = alpha * A
    ! - rci_op_scale(iS,task,m,n,alpha,Aidx,lda)

    ! A = A * Diag(resvec)
    ! - rci_op_colscale(iS,task,m,n,alphavec,Aidx,lda)

    ! A = Diag(resvec) * A
    ! - rci_op_rowscale(iS,task,m,n,alphavec,Aidx,lda)

    ! B = alpha * A + B
    ! - rci_op_axpy(iS,task,m,n,alpha,Aidx,lda,Bidx,ldb)

    ! resvec(1) = diag( A' * A )
    ! - rci_op_col_norm(iS,task,m,n,Aidx,lda)

    ! C = alpha * A^(trA) * B^(trB) + beta * C
    ! - rci_op_gemm(iS,task,trA,trB,m,n,k,
    !               alpha,Aidx,lda,Bidx,ldb,beta,Cidx,ldc)

    ! A * C = C * Diag(resvec)
    ! A will be rewritten by C
    ! - rci_op_heev(iS,task,jobz,uplo,n,Aidx,lda)

    ! A * C = B * C * Diag(resvec)
    ! A will be rewritten by C
    ! - rci_op_hegv(iS,task,jobz,uplo,n,Aidx,lda,Bidx,ldb)

    ! Cholesky factorization of A
    ! - rci_op_potrf(iS,task,uplo,m,Aidx,lda)

    ! B is the solution of op(A)*X = alpha*B or X*op(A) = alpha*B
    ! - rci_op_trsm(iS,task,side,uplo,trA,m,n,alpha,Aidx,lda,Bidx,ldb)


    subroutine rci_op_null(task)

        !**** FUNCTION ********************************!
        ! Not do anything

        implicit none

        !**** OUTPUT ***********************************!
        integer(i4), intent(out) :: task

        task = RCI_NULL

    end subroutine rci_op_null

    subroutine rci_op_stop(task)

        !**** FUNCTION ********************************!
        ! Not do anything

        implicit none

        !**** OUTPUT ***********************************!
        integer(i4), intent(out) :: task

        task = RCI_STOP

    end subroutine rci_op_stop

    subroutine rci_op_converge(task, conv_flag)

        !**** FUNCTION ********************************!
        ! Not do anything

        implicit none

        !**** INPUT ***********************************!
        logical, intent(in) :: conv_flag ! convergence flag

        !**** OUTPUT ***********************************!
        integer(i4), intent(out) :: task

        if (conv_flag) then
            task = RCI_CONVERGE
        else
            task = RCI_STOP
        end if

    end subroutine rci_op_converge

    subroutine rci_op_allocate(iS, task, m, n, Aidx)

        !**** FUNCTION ********************************!
        ! Allocate matrix A with size m by n

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of A
        integer(i4), intent(in) :: n ! width of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_ALLOCATE
        iS%Aidx = Aidx
        iS%m = m
        iS%n = n

    end subroutine rci_op_allocate

    subroutine rci_op_deallocate(iS, task, Aidx)

        !**** FUNCTION ********************************!
        ! Deallocate matrix A

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: Aidx ! indices for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_DEALLOCATE
        iS%Aidx = Aidx

    end subroutine rci_op_deallocate

    subroutine rci_op_h_multi(iS, task, trH, m, n, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = op(H) * A
        ! op() denotes the operation of normal(N), transpose(T),
        ! conjugate(C)

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: trH ! transpose of H
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_H_MULTI
        iS%TrH = trH
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_h_multi

    subroutine rci_op_s_multi(iS, task, trS, m, n, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = op(S) * A
        ! op() denotes the operation of normal(N), transpose(T),
        ! conjugate(C)

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: trS ! transpose of S
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_S_MULTI
        iS%TrS = trS
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_s_multi

    subroutine rci_op_p_multi(iS, task, trP, m, n, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = op(P) * A
        ! op() denotes the operation of normal(N), transpose(T),
        ! conjugate(C)

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: trP ! transpose of P
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_P_MULTI
        iS%TrP = trP
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_p_multi

    subroutine rci_op_copy(iS, task, trA, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = op(A)

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: trA ! transpose of A
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out)  :: iS
        integer(i4), intent(out) :: task

        task = RCI_COPY
        iS%trA = trA
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_copy

    subroutine rci_op_subcopy(iS, task, m, n, Aidx, lda, rAoff, cAoff, &
                              Bidx, ldb, rBoff, cBoff)

        !**** FUNCTION ********************************!
        ! B(rBoff+(1:m),cBoff+(1:n)) = A(rAoff+(1:m),cAoff+(1:n))

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B
        integer(i4), intent(in) :: lda, ldb ! lda for matrix A and B
        integer(i4), intent(in) :: m, n ! size of the submatrix
        integer(i4), intent(in) :: rAoff, cAoff ! offsets of A
        integer(i4), intent(in) :: rBoff, cBoff ! offsets of B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_SUBCOPY
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda
        iS%rAoff = rAoff
        iS%cAoff = cAoff
        iS%Bidx = Bidx
        iS%ldb = ldb
        iS%rBoff = rBoff
        iS%cBoff = cBoff

    end subroutine rci_op_subcopy

    subroutine rci_op_subcol(iS, task, m, n, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = A(:,logical(resvec))

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_SUBCOL
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_subcol

    subroutine rci_op_subrow(iS, task, m, n, Aidx, Bidx)

        !**** FUNCTION ********************************!
        ! B = A(logical(resvec),:)

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_SUBROW
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%Bidx = Bidx

    end subroutine rci_op_subrow

    subroutine rci_op_trace(iS, task, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! resvec(1) = trace(A)

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: n ! height of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_TRACE
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_trace

    subroutine rci_op_dot(iS, task, m, n, Aidx, lda, Bidx, ldb)

        !**** FUNCTION ********************************!
        ! resvec(1) = trace(A*B) = dot(A(:),B(:))

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of A
        integer(i4), intent(in) :: n ! width of A
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B
        integer(i4), intent(in) :: lda, ldb ! lda for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_DOT
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda
        iS%Bidx = Bidx
        iS%ldb = ldb

    end subroutine rci_op_dot

    subroutine rci_op_scale(iS, task, m, n, alpha, Aidx, lda)

        !**** FUNCTION ********************************!
        ! A = alpha*A

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        real(r8), intent(in) :: alpha
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda ! lda for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_SCALE
        iS%m = m
        iS%n = n
        iS%alpha = alpha
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_scale

    subroutine rci_op_colscale(iS, task, m, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! A = A * Diag(resvec)

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of A
        integer(i4), intent(in) :: n ! width of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda ! lda for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_COLSCALE
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_colscale

    subroutine rci_op_rowscale(iS, task, m, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! A = Diag(resvec) * A

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of A
        integer(i4), intent(in) :: n ! width of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda ! lda for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_ROWSCALE
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_rowscale

    subroutine rci_op_axpy(iS, task, m, n, alpha, Aidx, lda, Bidx, ldb)

        !**** FUNCTION ********************************!
        ! B = alpha * A + B

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of B
        integer(i4), intent(in) :: n ! width of B
        real(r8), intent(in) :: alpha
        integer(i4), intent(in) :: lda, ldb ! lda for matrix A and B
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A and B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_AXPY
        iS%m = m
        iS%n = n
        iS%alpha = alpha
        iS%Aidx = Aidx
        iS%lda = lda
        iS%Bidx = Bidx
        iS%ldb = ldb

    end subroutine rci_op_axpy

    subroutine rci_op_col_norm(iS, task, m, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! resvec(1) = diag( A' * A )

        implicit none

        !**** INPUT ***********************************!
        integer(i4), intent(in) :: m ! height of A
        integer(i4), intent(in) :: n ! width of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda ! indices for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_COL_NORM
        iS%m = m
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_col_norm

    subroutine rci_op_gemm(iS, task, trA, trB, m, n, k, &
                           alpha, Aidx, lda, Bidx, ldb, beta, Cidx, ldc)

        !**** FUNCTION ********************************!
        ! C = alpha * op(A)*op(B) + beta * C
        ! op() denotes the operation of normal(N), transpose(T),
        ! conjugate(C)

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: trA, trB ! transpose of A and B
        integer(i4), intent(in) :: m ! height of C
        integer(i4), intent(in) :: n ! width of C
        integer(i4), intent(in) :: k ! inner size of op(A)*op(B)
        real(r8), intent(in) :: alpha, beta
        integer(i4), intent(in) :: Aidx, Bidx, Cidx ! idx for mat A, B and C
        integer(i4), intent(in) :: lda, ldb, ldc ! lda for mat A, B and C

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_GEMM
        iS%trA = trA
        iS%trB = trB
        iS%m = m
        iS%n = n
        iS%k = k
        iS%alpha = alpha
        iS%Aidx = Aidx
        iS%lda = lda
        iS%Bidx = Bidx
        iS%ldb = ldb
        iS%beta = beta
        iS%Cidx = Cidx
        iS%ldc = ldc

    end subroutine rci_op_gemm

    subroutine rci_op_heev(iS, task, jobz, uplo, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! A * C = C * Diag(resvec)
        ! A will be rewrite by C

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: jobz ! with/without eigenvector
        character, intent(in) :: uplo ! upper or lower part of A
        integer(i4), intent(in) :: n ! size of A
        integer(i4), intent(in) :: Aidx ! indices for matrix A
        integer(i4), intent(in) :: lda ! lda for matrix A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_HEEV
        iS%jobz = jobz
        iS%uplo = uplo
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_heev

    subroutine rci_op_hegv(iS, task, jobz, uplo, n, Aidx, lda, Bidx, ldb)

        !**** FUNCTION ********************************!
        ! A * C = B * C * Diag(resvec)
        ! A will be rewrite by C

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: jobz ! with/without eigenvector
        character, intent(in) :: uplo ! upper or lower part of A
        integer(i4), intent(in) :: n ! size of A
        integer(i4), intent(in) :: Aidx, Bidx ! indices for matrix A, B
        integer(i4), intent(in) :: lda, ldb ! lda for matrix A, B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_HEGV
        iS%jobz = jobz
        iS%uplo = uplo
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda
        iS%Bidx = Bidx
        iS%ldb = ldb

    end subroutine rci_op_hegv

    subroutine rci_op_potrf(iS, task, uplo, n, Aidx, lda)

        !**** FUNCTION ********************************!
        ! Cholesky factorization of A
        ! uplo denote lower or upper resulting factor

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: uplo ! upper or lower
        integer(i4), intent(in) :: n ! size of A
        integer(i4), intent(in) :: Aidx ! idx for mat A
        integer(i4), intent(in) :: lda ! lda for mat A

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_POTRF
        iS%uplo = uplo
        iS%m = n
        iS%n = n
        iS%Aidx = Aidx
        iS%lda = lda

    end subroutine rci_op_potrf

    subroutine rci_op_trsm(iS, task, side, uplo, trA, m, n, &
            alpha, Aidx, lda, Bidx, ldb)

        !**** FUNCTION ********************************!
        ! Triangular matrix solve
        ! op(A)*X = alpha*B or X*op(A) = alpha*B
        ! B is over written by X

        implicit none

        !**** INPUT ***********************************!
        character, intent(in) :: side ! side
        character, intent(in) :: uplo ! upper or lower
        character, intent(in) :: trA ! transpose of A
        integer(i4), intent(in) :: m, n ! size of B
        real(r8), intent(in) :: alpha
        integer(i4), intent(in) :: Aidx, Bidx ! idx for mat A, B
        integer(i4), intent(in) :: lda, ldb ! lda for mat A, B

        !**** OUTPUT ***********************************!
        type(rci_instr), intent(out) :: iS
        integer(i4), intent(out) :: task

        task = RCI_TRSM
        iS%side = side
        iS%uplo = uplo
        iS%trA = trA
        iS%m = m
        iS%n = n
        iS%alpha = alpha
        iS%Aidx = Aidx
        iS%lda = lda
        iS%Bidx = Bidx
        iS%ldb = ldb

    end subroutine rci_op_trsm

end module ELSI_RCI_OPS
