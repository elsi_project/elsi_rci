! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains operations used within RCI.
!!
module ELSI_RCI_SETUP

    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:r8, i4

    implicit none

contains

    subroutine rci_init(r_h, solver, n_basis, n_state, tol_iter, &
            max_iter, verbose)

        implicit none

        type(rci_handle), intent(out) :: r_h !< Handle
        integer(i4), intent(in)       :: n_basis !< Number of basis
        integer(i4), intent(in)       :: n_state !< Number of states
        integer(i4), intent(in),optional       :: solver !< Refer to RCI_CONSTANTS
        real(r8), intent(in),optional          :: tol_iter !< Number of basis
        integer(i4), intent(in),optional       :: max_iter !< Number of states
        integer(i4), intent(in),optional       :: verbose !< output

        if(present(solver)) r_h%solver = solver

        r_h%n_basis = n_basis
        r_h%n_state = n_state

        if(present(tol_iter)) r_h%tol_iter = tol_iter
        if(present(max_iter)) r_h%max_iter = max_iter

        r_h%total_energy = 0.0_r8
        r_h%total_iter = 0

        r_h%verbose = verbose

        ! OMM default
        r_h%omm_est_ub = 20

        ! PPCG default
        r_h%ppcg_tol_lock = tol_iter/10
        r_h%ppcg_sbsize = min(8,n_state)
        r_h%ppcg_rrstep = min(5,max_iter)

        ! ChebFilter default
        r_h%cheb_est_lb = 0.25
        r_h%cheb_est_ub = 20
        r_h%cheb_max_inneriter = 10

        select case (solver)
        case (RCI_SOLVER, RCI_SOLVER_DAVIDSON)
            r_h%n_res = min(4*n_state,n_basis)
            r_h%max_n = min(4*n_state,n_basis)
        case (RCI_SOLVER_OMM)
            r_h%n_res = n_state
            r_h%max_n = n_state
        case (RCI_SOLVER_PPCG)
            r_h%n_res = max(n_state,3*r_h%ppcg_sbsize)
            r_h%max_n = max(n_state,3*r_h%ppcg_sbsize)
        case (RCI_SOLVER_CHEBFILTER)
            r_h%n_res = n_state
            r_h%max_n = n_state
        end select

    end subroutine

end module ELSI_RCI_SETUP
