! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to solve an eigenproblem using a reverse
!! communication interface.
!!
module ELSI_RCI_CHEBFILTER

    use ELSI_RCI_OPS
    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:i4

    implicit none

    public :: rci_chebfilter
    public :: rci_chebfilter_allocate
    public :: rci_chebfilter_deallocate

    ! Matrix ID of size m by n
    !&<
    integer(i4), parameter :: MID_Psi       = 1
    integer(i4), parameter :: MID_Psiold    = 2
    integer(i4), parameter :: MID_HPsi      = 3
    integer(i4), parameter :: MID_WORK      = 4
    integer(i4), parameter :: MID_Psi_lock  = 5
    integer(i4), parameter :: MID_HPsi_lock = 6
    !&>

    ! Matrix ID of size n by n
    !&<
    integer(i4), parameter :: MID_HR     = 21
    integer(i4), parameter :: MID_SR     = 22
    integer(i4), parameter :: MID_VR     = 23
    integer(i4), parameter :: MID_WORK1  = 24
    !&>

    ! Stage ID
    !&<
    integer(i4), parameter :: SID_INIT        = 0
    integer(i4), parameter :: SID_INNERITER   = 200
    integer(i4), parameter :: SID_ORTH        = 300
    integer(i4), parameter :: SID_LOCK        = 400
    integer(i4), parameter :: SID_FINISH      = 500
    integer(i4), parameter :: SID_ALLOCATE    = 600
    integer(i4), parameter :: SID_DEALLOCATE  = 700
    !&>

contains

    subroutine rci_chebfilter_allocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.
        integer(i4), save :: m, n ! size of the wave functions.

        if (ijob <= SID_ALLOCATE) then
            ijob = SID_ALLOCATE + 1
            m = r_h%n_basis
            n = r_h%max_n
            iter = 0
        end if

        ! Allocate matrix of size m by n
        if (ijob == SID_ALLOCATE + 1) then
            iter = iter + 1
            if (iter > 6) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_allocate(iS, task, m, n, iter)
            end if
            return
        end if

        ! Allocate matrix of size n by n
        if (ijob == SID_ALLOCATE + 2) then
            iter = iter + 1
            if (iter > 24) then
                call rci_op_stop(task)
            else
                call rci_op_allocate(iS, task, n, n, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_chebfilter_deallocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.

        if (ijob <= SID_DEALLOCATE) then
            ijob = SID_DEALLOCATE + 1
            iter = 0
        end if

        ! Deallocate matrix of size m by n
        if (ijob == SID_DEALLOCATE + 1) then
            iter = iter + 1
            if (iter > 6) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

        ! Deallocate matrix of size n by n
        if (ijob == SID_DEALLOCATE + 2) then
            iter = iter + 1
            if (iter > 24) then
                call rci_op_stop(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_chebfilter(r_h, ijob, iS, task, resvec)

        implicit none

        !**** INPUT ***********************************!

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        type(rci_handle), intent(inout) :: r_h
        integer(i4), intent(inout) :: ijob ! job id
        real(r8), intent(inout) :: resvec(:)
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4) :: it, iti
        integer(i4), save :: m ! size of basis
        integer(i4), save :: n ! number of current states
        integer(i4), save :: nact ! number of active states
        integer(i4), save :: n_state ! number of wanted states
        integer(i4), save :: niter ! iteration num.
        integer(i4), save :: ninneriter ! inner iteration num.
        integer(i4), save :: max_iter ! max number of iterations
        integer(i4), save :: max_inneriter ! max number of inner iterations

        real(r8), save :: tol_iter ! convergence tolerance
        real(r8), save :: c ! center of spectrum
        real(r8), save :: r ! radius of spectrum

        real(r8), save, allocatable :: Vec_conv(:)
        real(r8), save, allocatable :: Vec_ev(:)
        real(r8), save, allocatable :: Vec_evold(:)

        logical, save :: ovlp_is_unit ! ovlp is unit flag
        logical :: convflag ! convergence flag.
        !**********************************************!

        ! -- Init: parameter setup
        if (ijob <= SID_INIT) then
            ijob = SID_INNERITER

            m = r_h%n_basis
            n = r_h%n_state
            nact = r_h%n_state
            n_state = r_h%n_state
            ovlp_is_unit = r_h%ovlp_is_unit
            max_iter = r_h%max_iter
            max_inneriter = r_h%cheb_max_inneriter
            tol_iter = r_h%tol_iter
            niter = 0
            ninneriter = 0
            c = (r_h%cheb_est_lb+r_h%cheb_est_ub)/2.0_r8
            r = (r_h%cheb_est_ub-r_h%cheb_est_lb)/2.0_r8

            allocate (Vec_conv(n))
            Vec_conv = 0.0_r8
            allocate (Vec_ev(n))
            Vec_ev = 0.0_r8
            allocate (Vec_evold(n))
            Vec_evold = 0.0_r8

            call rci_op_h_multi(iS, task, 'N', m, nact, MID_Psi, MID_HPsi)
            return
        end if

        ! -- start inner iteration
        if (ijob == SID_INNERITER) then
            ninneriter = 0
            call rci_op_scale(iS, task, m, nact, 0.0_r8, MID_Psiold, m)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = H*Psi
        if (ijob == SID_INNERITER + 1) then
            ninneriter = ninneriter + 1
            if (ninneriter > 1) then
                call rci_op_h_multi(iS, task, 'N', m, nact, &
                    MID_Psi, MID_WORK)
            else
                call rci_op_copy(iS, task, 'N', MID_HPsi, MID_WORK)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = S^-1*H*Psi
        if (ijob == SID_INNERITER + 2) then
            call rci_op_p_multi(iS, task, 'N', m, nact, MID_WORK, &
                                MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = HPsi - c*Psi
        if (ijob == SID_INNERITER + 3) then
            call rci_op_axpy(iS, task, m, nact, -c, MID_Psi, m, MID_HPsi, m)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = 2/r*HPsi
        if (ijob == SID_INNERITER + 4) then
            call rci_op_scale(iS, task, m, nact, 2.0_r8/r, MID_HPsi, m)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = HPsi - Psiold
        if (ijob == SID_INNERITER + 5) then
            call rci_op_axpy(iS, task, m, nact, -1.0_r8, &
                MID_Psiold, m, MID_HPsi, m)
            ijob = ijob + 1
            return
        end if

        ! -- Psiold = Psi
        if (ijob == SID_INNERITER + 6) then
            call rci_op_copy(iS, task, 'N', MID_Psi, MID_Psiold)
            ijob = ijob + 1
            return
        end if

        ! -- Psi = HPsi
        if (ijob == SID_INNERITER + 7) then
            call rci_op_copy(iS, task, 'N', MID_HPsi, MID_Psi)
            ijob = ijob + 1
            return
        end if

        if (ijob == SID_INNERITER + 8) then
            call rci_op_null(task)
            if (ninneriter < max_inneriter) then
                ijob = SID_INNERITER + 1
            else
                ijob = SID_ORTH
            end if
            return
        end if

        ! -- HPsi = H*Psi
        if (ijob == SID_ORTH) then
            call rci_op_h_multi(iS, task, 'N', m, nact, MID_Psi, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi = [HPsi HPsi_lock]
        if (ijob == SID_ORTH + 1) then
            call rci_op_subcopy(iS, task, m, n-nact, &
                MID_HPsi_lock, m, 0, 0, &
                MID_HPsi, m, 0, nact)
            ijob = ijob + 1
            return
        end if

        ! -- Psi = [Psi Psi_lock]
        if (ijob == SID_ORTH + 2) then
            call rci_op_subcopy(iS, task, m, n-nact, &
                MID_Psi_lock, m, 0, 0, &
                MID_Psi, m, 0, nact)
            ijob = ijob + 1
            return
        end if

        ! -- SPsi = S*Psi
        if (ijob == SID_ORTH + 3) then
            if (ovlp_is_unit) then
                call rci_op_copy(iS, task, 'N', MID_Psi, MID_WORK)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_Psi, MID_WORK)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HR = Psi'*HPsi
        if (ijob == SID_ORTH + 4) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_Psi, m, MID_HPsi, m, 0.0_r8, &
                             MID_HR, n)
            ijob = ijob + 1
            return
        end if

        ! -- SR = Psi'*SPsi
        if (ijob == SID_ORTH + 5) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_Psi, m, MID_WORK, m, 0.0_r8, &
                             MID_SR, n)
            ijob = ijob + 1
            return
        end if

        ! -- HR * VR = SR * VR * Diag(EW)
        ! - calculate reduced generalized eigenvalue problem
        if (ijob == SID_ORTH + 6) then
            call rci_op_copy(iS, task, 'N', MID_HR, MID_VR)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ORTH + 7) then
            call rci_op_copy(iS, task, 'N', MID_SR, MID_WORK1)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ORTH + 8) then
            call rci_op_hegv(iS, task, 'V', 'L', n, &
                             MID_VR, n, MID_WORK1, n)
            ijob = ijob + 1
            return
        end if

        ! - Psi = Psi * VR
        if (ijob == SID_ORTH + 9) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n, n, 1.0_r8, &
                             MID_Psi, m, MID_VR, n, 0.0_r8, &
                             MID_WORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ORTH + 10) then
            call rci_op_copy(iS, task, 'N', MID_WORK, MID_Psi)
            ijob = ijob + 1
            return
        end if

        ! - HPsi = HPsi * VR
        if (ijob == SID_ORTH + 11) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n, n, 1.0_r8, &
                             MID_HPsi, m, MID_VR, n, 0.0_r8, &
                             MID_WORK, m)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ORTH + 12) then
            call rci_op_copy(iS, task, 'N', MID_WORK, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! -store old ev for convergence checking
        if (ijob == SID_ORTH + 13) then
            call rci_op_null(task)
            niter = niter + 1
            if (niter == 1) then
                Vec_evold = resvec
                ijob = SID_INNERITER
                return
            else
                Vec_ev = resvec
                do it = 1, n_state
                    if (abs((Vec_ev(it) - Vec_evold(it))/Vec_ev(1)) &
                        < tol_iter) then
                        Vec_conv(it) = 1.0_r8
                    else
                        Vec_conv(it) = 0.0_r8
                    end if
                end do

                if (r_h%verbose > 0) write(*,'(a,i5,a,es15.7e3)') &
                    'Iter ', niter, ', Rel Err: ', &
                    sum(abs((Vec_ev(1:n_state) &
                    - Vec_evold(1:n_state))/Vec_ev(1)))

                Vec_evold = Vec_ev

                if (n_state == int(sum(Vec_conv), i4)) then
                    ijob = SID_FINISH
                else if (niter > max_iter) then
                    ijob = SID_FINISH
                else
                    ijob = SID_LOCK
                end if
                return
            end if
        end if

        ! -------------------------------------------------
        ! - lock converged eigen pairs
        ! Psi_lock = Psi(:, conv)
        if (ijob == SID_LOCK) then
            resvec = Vec_conv
            nact = n - sum(Vec_conv)
            call rci_op_subcol(iS, task, m, n, MID_Psi, MID_Psi_lock)
            ijob = ijob + 1
            return
        end if

        ! HPsi_lock = HPsi(:, conv)
        if (ijob == SID_LOCK + 1) then
            call rci_op_subcol(iS, task, m, n, MID_HPsi, MID_HPsi_lock)
            ijob = ijob + 1
            return
        end if

        ! Psi = Psi(:, not conv)]
        if (ijob == SID_LOCK + 2) then
            resvec = 1.0_r8 - Vec_conv
            call rci_op_subcol(iS, task, m, n, MID_Psi, MID_WORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 3) then
            call rci_op_copy(iS, task, 'N', MID_WORK, MID_Psi)
            ijob = ijob + 1
            return
        end if

        ! HPsi = HPsi(:, not conv)]
        if (ijob == SID_LOCK + 4) then
            call rci_op_subcol(iS, task, m, n, MID_HPsi, MID_WORK)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_LOCK + 5) then
            call rci_op_copy(iS, task, 'N', MID_WORK, MID_HPsi)
            ijob = SID_INNERITER
            return
        end if

        ! - Finish the iteration
        ! - Converge or Stop
        if (ijob == SID_FINISH) then
            r_h%total_energy = sum(Vec_ev(1:n_state))
            r_h%total_iter = niter
            if (r_h%verbose > 0) then
                write(*,*) 'Eigenvalues are:'
                print *, Vec_ev(1:n_state)
            end if
            convflag = (n_state == int(sum(Vec_conv)))
            deallocate (Vec_conv)
            deallocate (Vec_ev)
            deallocate (Vec_evold)
            call rci_op_converge(task, convflag)
            return
        end if

    end subroutine

end module ELSI_RCI_CHEBFILTER
