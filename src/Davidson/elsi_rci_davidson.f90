! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to solve an eigenproblem using a reverse
!! communication interface.
!!
module ELSI_RCI_DAVIDSON

    use ELSI_RCI_OPS
    use ELSI_RCI_CONSTANTS
    use ELSI_RCI_DATATYPE
    use ELSI_RCI_PRECISION, only:i4

    implicit none

    public :: rci_davidson
    public :: rci_davidson_allocate
    public :: rci_davidson_deallocate

    ! Matrix ID of size m by n
    !&<
    integer(i4), parameter :: MID_Psi     = 1
    integer(i4), parameter :: MID_PsiExt  = 2
    integer(i4), parameter :: MID_HPsi    = 3
    integer(i4), parameter :: MID_HPsiExt = 4
    integer(i4), parameter :: MID_SPsiExt = 5 ! also serve temp space
    integer(i4), parameter :: MID_SPsi    = 6
    !&>

    ! Matrix ID of size n by n
    !&<
    integer(i4), parameter :: MID_HR     = 21
    integer(i4), parameter :: MID_SR     = 22
    integer(i4), parameter :: MID_VR     = 23
    integer(i4), parameter :: MID_VRsub  = 24
    integer(i4), parameter :: MID_WORK1  = 25
    integer(i4), parameter :: MID_WORK2  = 26
    !&>

    ! Stage ID
    !&<
    integer(i4), parameter :: SID_INIT        = 0
    integer(i4), parameter :: SID_ITER        = 100
    integer(i4), parameter :: SID_PROJ        = 200
    integer(i4), parameter :: SID_RECONST     = 300
    integer(i4), parameter :: SID_RESTART     = 400
    integer(i4), parameter :: SID_FINISH      = 500
    integer(i4), parameter :: SID_ALLOCATE    = 600
    integer(i4), parameter :: SID_DEALLOCATE  = 700
    !&>

contains

    subroutine rci_davidson_allocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.
        integer(i4), save :: m, n ! size of the wave functions.

        if (ijob <= SID_ALLOCATE) then
            ijob = SID_ALLOCATE + 1
            m = r_h%n_basis
            n = r_h%max_n
            iter = 0
        end if

        ! Allocate matrix of size m by n
        if (ijob == SID_ALLOCATE + 1) then
            iter = iter + 1
            if (iter > 6) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 5) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_allocate(iS, task, m, n, iter)
            end if
            return
        end if

        ! Allocate matrix of size n by n
        if (ijob == SID_ALLOCATE + 2) then
            iter = iter + 1
            if (iter > 26) then
                call rci_op_stop(task)
            else
                call rci_op_allocate(iS, task, n, n, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_davidson_deallocate(r_h, ijob, iS, task)

        implicit none

        !**** INPUT ***********************************!
        type(rci_handle), intent(in) :: r_h

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        integer(i4), intent(inout) :: ijob ! job id
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4), save :: iter ! step num.

        if (ijob <= SID_DEALLOCATE) then
            ijob = SID_DEALLOCATE + 1
            iter = 0
        end if

        ! Deallocate matrix of size m by n
        if (ijob == SID_DEALLOCATE + 1) then
            iter = iter + 1
            if (iter > 6) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            elseif ((iter > 5) .and. (r_h%ovlp_is_unit)) then
                ijob = ijob + 1
                iter = 20
                call rci_op_null(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

        ! Deallocate matrix of size n by n
        if (ijob == SID_DEALLOCATE + 2) then
            iter = iter + 1
            if (iter > 26) then
                call rci_op_stop(task)
            else
                call rci_op_deallocate(iS, task, iter)
            end if
            return
        end if

    end subroutine

    subroutine rci_davidson(r_h, ijob, iS, task, resvec)

        implicit none

        !**** INPUT ***********************************!

        !**** OUTPUT **********************************!
        integer(i4), intent(out) :: task

        !**** INOUT ***********************************!
        type(rci_handle), intent(inout) :: r_h
        integer(i4), intent(inout) :: ijob ! job id
        real(r8), intent(inout) :: resvec(:)
        type(rci_instr), intent(inout) :: iS

        !**** LOCAL ***********************************!
        integer(i4) :: it, iti
        integer(i4), save :: m ! size of basis
        integer(i4), save :: n ! number of current states
        integer(i4), save :: n_state ! number of wanted states
        integer(i4), save :: n_restart ! number of restarted states
        integer(i4), save :: next ! number of extended states
        integer(i4), save :: niter ! iteration num.
        integer(i4), save :: max_iter ! max number of iterations
        integer(i4), save :: max_n ! max number of basis in reduced space

        real(r8), save :: tol_iter ! convergence tolerance

        logical, save :: ovlp_is_unit ! ovlp is unit flag

        real(r8), save, allocatable :: Vec_conv(:)
        real(r8), save, allocatable :: Vec_ev(:)
        real(r8), save, allocatable :: Vec_evsub(:)
        real(r8), save, allocatable :: Vec_evold(:)
        !**********************************************!

        ! -- Init: parameter setup
        if (ijob <= SID_INIT) then
            ijob = SID_INIT + 1

            m = r_h%n_basis
            n = r_h%n_state
            n_state = r_h%n_state
            ovlp_is_unit = r_h%ovlp_is_unit
            max_iter = r_h%max_iter
            tol_iter = r_h%tol_iter
            max_n = r_h%max_n
            niter = 0
            next = 0

            allocate (Vec_conv(max_n))
            Vec_conv = 0.0_r8
            allocate (Vec_ev(max_n))
            Vec_ev = 0.0_r8
            allocate (Vec_evsub(max_n))
            Vec_evsub = 0.0_r8
            allocate (Vec_evold(max_n))
            Vec_evold = 0.0_r8

            call rci_op_null(task)
            return
        end if

        ! -- HPsi = H*Psi
        if (ijob == SID_INIT + 1) then
            call rci_op_h_multi(iS, task, 'N', m, n, MID_Psi, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! -- SPsi = S*Psi
        if (ijob == SID_INIT + 2) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, n, MID_Psi, MID_SPsi)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HR = Psi'*HPsi
        if (ijob == SID_INIT + 3) then
            call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                             MID_Psi, m, MID_HPsi, m, 0.0_r8, &
                             MID_HR, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- SR = Psi'*SPsi
        if (ijob == SID_INIT + 4) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_Psi, m, MID_Psi, m, 0.0_r8, &
                                 MID_SR, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, n, m, 1.0_r8, &
                                 MID_Psi, m, MID_SPsi, m, 0.0_r8, &
                                 MID_SR, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- HR * VR = SR * VR * Diag(EW)
        ! - calculate reduced generalized eigenvalue problem
        if (ijob == SID_INIT + 5) then
            call rci_op_copy(iS, task, 'N', MID_HR, MID_VR)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_INIT + 6) then
            call rci_op_copy(iS, task, 'N', MID_SR, MID_WORK1)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_INIT + 7) then
            call rci_op_hegv(iS, task, 'V', 'L', n, &
                             MID_VR, max_n, MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -store old ev for convergence checking
        if (ijob == SID_INIT + 8) then
            Vec_ev = resvec
            Vec_evold = resvec
            next = min(n,max_n-n)
            call rci_op_null(task)
            ijob = SID_ITER
            return
        end if

        !-------------------------------------------------
        ! This is the main loop of the Davidson algorithm.

        ! - Redistribution of the vectors
        ! VRsub = VR(:,not Vec_conv)
        if (ijob == SID_ITER) then
            resvec = 1.0_r8 - Vec_conv
            call rci_op_subcol(iS, task, max_n, n_state, MID_VR, MID_VRsub)
            ijob = ijob + 1
            return
        end if

        ! EVsub = EV(not Vec_conv)
        if (ijob == SID_ITER + 1) then
            iti = 0
            do it = 1, n_state
                if (Vec_conv(it) < 0.5_r8) then
                    iti = iti + 1
                    Vec_evsub(iti) = Vec_ev(it)
                end if
            end do
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = SPsi * VRsub
        if (ijob == SID_ITER + 2) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'N', 'N', m, next, n, 1.0_r8, &
                                 MID_Psi, m, MID_VRsub, max_n, 0.0_r8, &
                                 MID_SPsiExt, m)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, next, n, 1.0_r8, &
                                 MID_SPsi, m, MID_VRsub, max_n, 0.0_r8, &
                                 MID_SPsiExt, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = SPsiExt * Diag(EWsub)
        if (ijob == SID_ITER + 3) then
            resvec = Vec_evsub
            call rci_op_colscale(iS, task, m, next, MID_SPsiExt, m)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = HPsi * VRsub - SPsiExt
        if (ijob == SID_ITER + 4) then
            call rci_op_gemm(iS, task, 'N', 'N', m, next, n, 1.0_r8, &
                             MID_HPsi, m, MID_VRsub, max_n, -1.0_r8, &
                             MID_SPsiExt, m)
            ijob = SID_PROJ
            return
        end if

        ! - approx solve(SPsiExt)
        ! This is a special function only exist for Davidson method
        if (ijob == SID_PROJ) then
            resvec = Vec_evsub
            call rci_op_p_multi(iS, task, 'N', m, next, MID_SPsiExt, &
                                MID_PsiExt)
            ijob = ijob + 1
            return
        end if

        ! - Work1 = Psi'*Psi
        if (ijob == SID_PROJ + 1) then
            call rci_op_copy(iS, task, 'N', MID_PsiExt, MID_SPsiExt)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_PROJ + 2) then
            call rci_op_gemm(iS, task, 'C', 'N', next, next, &
                             m, 1.0_r8, &
                             MID_PsiExt, m, MID_SPsiExt, m, 0.0_r8, &
                             MID_Work1, max_n)
            ijob = ijob + 1
            return
        end if

        ! - Cholesky factorization of G
        if (ijob == SID_PROJ + 3) then
            call rci_op_potrf(iS, task, 'U', next, MID_Work1, max_n)
            ijob = ijob + 1
            return
        end if

        ! - PsiExt = PsiExt/Work1
        if (ijob == SID_PROJ + 4) then
            call rci_op_trsm(iS, task, 'R', 'U', 'N', m, next, 1.0_r8, &
                             MID_Work1, max_n, MID_PsiExt, m)
            ijob = SID_RECONST
            return
        end if

!        ! - norm(PsiExt)
!        if (ijob == SID_PROJ + 1) then
!            call rci_op_col_norm(iS, task, m, next, MID_PsiExt, m)
!            ijob = ijob + 1
!            return
!        end if
!
!        ! - normize(PsiExt)
!        if (ijob == SID_PROJ + 2) then
!            do it = 1, next
!                if (resvec(it) > 0) then
!                    resvec(it) = 1.0_r8/resvec(it)
!                else
!                    resvec(it) = 0.0_r8
!                end if
!            end do
!            call rci_op_colscale(iS, task, m, next, MID_PsiExt, m)
!            ijob = SID_RECONST
!            return
!        end if

        ! -- HPsiExt = H * PsiExt
        if (ijob == SID_RECONST) then
            call rci_op_h_multi(iS, task, 'N', m, next, MID_PsiExt, &
                                MID_HPsiExt)
            ijob = ijob + 1
            return
        end if

        ! -- HR12 = Psi' * HPsiExt
        if (ijob == SID_RECONST + 1) then
            call rci_op_gemm(iS, task, 'C', 'N', n, next, m, 1.0_r8, &
                             MID_Psi, m, MID_HPsiExt, m, 0.0_r8, &
                             MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- HR(1:n, n+(1:next)) = HR12
        if (ijob == SID_RECONST + 2) then
            call rci_op_subcopy(iS, task, n, next, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_HR, max_n, 0, n)
            ijob = ijob + 1
            return
        end if

        ! -- HR21 = HPsiExt' * Psi
        if (ijob == SID_RECONST + 3) then
            call rci_op_gemm(iS, task, 'C', 'N', next, n, m, 1.0_r8, &
                             MID_HPsiExt, m, MID_Psi, m, 0.0_r8, &
                             MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- HR(n+(1:next), 1:n) = HR21
        if (ijob == SID_RECONST + 4) then
            call rci_op_subcopy(iS, task, next, n, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_HR, max_n, n, 0)
            ijob = ijob + 1
            return
        end if

        ! -- HR22 = PsiExt' * HPsiExt
        if (ijob == SID_RECONST + 5) then
            call rci_op_gemm(iS, task, 'C', 'N', next, next, m, 1.0_r8, &
                             MID_PsiExt, m, MID_HPsiExt, m, 0.0_r8, &
                             MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- HR(n+(1:next), n+(1:next)) = HR22
        if (ijob == SID_RECONST + 6) then
            call rci_op_subcopy(iS, task, next, next, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_HR, max_n, n, n)
            ijob = ijob + 1
            return
        end if

        ! -- SPsiExt = S*PsiExt
        if (ijob == SID_RECONST + 7) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_s_multi(iS, task, 'N', m, next, &
                                    MID_PsiExt, MID_SPsiExt)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- SR12 = Psi' * SPsiExt
        if (ijob == SID_RECONST + 8) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', n, next, m, 1.0_r8, &
                                 MID_Psi, m, MID_PsiExt, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', n, next, m, 1.0_r8, &
                                 MID_Psi, m, MID_SPsiExt, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- SR(1:n, n+(1:next)) = SR12
        if (ijob == SID_RECONST + 9) then
            call rci_op_subcopy(iS, task, n, next, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_SR, max_n, 0, n)
            ijob = ijob + 1
            return
        end if

        ! -- SR21 = SPsiExt' * Psi
        if (ijob == SID_RECONST + 10) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', next, n, m, 1.0_r8, &
                                 MID_PsiExt, m, MID_Psi, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', next, n, m, 1.0_r8, &
                                 MID_SPsiExt, m, MID_Psi, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- SR(n+(1:next),1:n) = SR21
        if (ijob == SID_RECONST + 11) then
            call rci_op_subcopy(iS, task, next, n, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_SR, max_n, n, 0)
            ijob = ijob + 1
            return
        end if

        ! -- SR22 = PsiExt' * SPsiExt
        if (ijob == SID_RECONST + 12) then
            if (ovlp_is_unit) then
                call rci_op_gemm(iS, task, 'C', 'N', next, next, m, 1.0_r8, &
                                 MID_PsiExt, m, MID_PsiExt, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            else
                call rci_op_gemm(iS, task, 'C', 'N', next, next, m, 1.0_r8, &
                                 MID_PsiExt, m, MID_SPsiExt, m, 0.0_r8, &
                                 MID_WORK1, max_n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -- SR(n+(1:next), n+(1:next)) = SR22
        if (ijob == SID_RECONST + 13) then
            call rci_op_subcopy(iS, task, next, next, &
                                MID_WORK1, max_n, 0, 0, &
                                MID_SR, max_n, n, n)
            ijob = SID_ITER + 20
            return
        end if

        ! -- HR * VR = SR * VR * Diag(EW)
        ! - calculate reduced generalized eigenvalue problem
        if (ijob == SID_ITER + 20) then
            call rci_op_copy(iS, task, 'N', MID_HR, MID_VR)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ITER + 21) then
            call rci_op_copy(iS, task, 'N', MID_SR, MID_WORK1)
            ijob = ijob + 1
            return
        end if
        if (ijob == SID_ITER + 22) then
            niter = niter + 1
            call rci_op_hegv(iS, task, 'V', 'L', n + next, &
                             MID_VR, max_n, MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- Psi(1:m, n+(1:next)) = PsiExt
        if (ijob == SID_ITER + 23) then
            Vec_ev = resvec
            call rci_op_subcopy(iS, task, m, next, &
                                MID_PsiExt, m, 0, 0, &
                                MID_Psi, m, 0, n)
            ijob = ijob + 1
            return
        end if

        ! -- HPsi(1:m, n+(1:next)) = HPsiExt
        if (ijob == SID_ITER + 24) then
            call rci_op_subcopy(iS, task, m, next, &
                                MID_HPsiExt, m, 0, 0, &
                                MID_HPsi, m, 0, n)
            ijob = ijob + 1
            return
        end if

        ! -- SPsi(1:m, n+(1:next)) = SPsiExt
        if (ijob == SID_ITER + 25) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_subcopy(iS, task, m, next, &
                                    MID_SPsiExt, m, 0, 0, &
                                    MID_SPsi, m, 0, n)
            end if
            ijob = ijob + 1
            return
        end if

        ! -store old ev for convergence checking
        if (ijob == SID_ITER + 26) then
            do it = 1, n_state
                if (abs((Vec_ev(it) - Vec_evold(it))/Vec_ev(1)) &
                    < tol_iter) then
                    Vec_conv(it) = 1.0_r8
                else
                    Vec_conv(it) = 0.0_r8
                end if
            end do
            n = n + next
            next = n_state - int(sum(Vec_conv(1:n)), i4)

            if (r_h%verbose > 0) write(*,'(a,i5,a,es15.7e3)') &
                'Iter ', niter, ', Rel Err: ', sum(abs((Vec_ev(1:n_state) &
                - Vec_evold(1:n_state))/Vec_ev(1)))

            Vec_evold = Vec_ev

            call rci_op_null(task)
            if (next == 0 .or. niter >= max_iter) then
                ijob = SID_FINISH
                return
            else if ( n + next > max_n ) then
                ijob = SID_RESTART
                return
            else
                ijob = SID_ITER
                return
            end if
        end if

        ! - Restart the iteration
        if (ijob == SID_RESTART) then
            resvec = Vec_conv
            n_restart = next
            do it = 1, n
                if (Vec_conv(it) < 0.5_r8) then
                    resvec(it) = 1.0_r8
                    next = next - 1
                end if
                if (next == 0) then
                    exit
                end if
            end do
            call rci_op_subcol(iS, task, max_n, n_state, MID_VR, MID_VRsub)
            ijob = ijob + 1
            return
        end if

        ! Reset Vec_conv
        if (ijob == SID_RESTART + 1) then
            iti = 0
            do it = 1, n
                if (resvec(it) > 0.5_r8) then
                    iti = iti + 1
                    Vec_ev(iti) = Vec_ev(it)
                end if
            end do
            next = n_restart
            do it = 1, n_state
                if ((next > 0) .and. (Vec_conv(it) < 0.5_r8)) then
                    next = next - 1
                else
                    Vec_conv(it) = 1.0_r8
                end if
            end do
            Vec_conv(n_state+1:max_n) = 0.0_r8
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! - PsiExt = Psi * VRsub
        if (ijob == SID_RESTART + 2) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n_state, n, 1.0_r8, &
                             MID_Psi, m, MID_VRsub, max_n, 0.0_r8, &
                             MID_PsiExt, m)
            ijob = ijob + 1
            return
        end if

        ! - Psi = PsiExt
        if (ijob == SID_RESTART + 3) then
            call rci_op_copy(iS, task, 'N', MID_PsiExt, MID_Psi)
            ijob = ijob + 1
            return
        end if

        ! - HPsiExt = HPsi * VRsub
        if (ijob == SID_RESTART + 4) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n_state, n, 1.0_r8, &
                             MID_HPsi, m, MID_VRsub, max_n, 0.0_r8, &
                             MID_HPsiExt, m)
            ijob = ijob + 1
            return
        end if

        ! - HPsi = HPsiExt
        if (ijob == SID_RESTART + 5) then
            call rci_op_copy(iS, task, 'N', MID_HPsiExt, MID_HPsi)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = SPsi * VRsub
        if (ijob == SID_RESTART + 6) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_gemm(iS, task, 'N', 'N', m, n_state, n, 1.0_r8, &
                                 MID_SPsi, m, MID_VRsub, max_n, 0.0_r8, &
                                 MID_SPsiExt, m)
            end if
            ijob = ijob + 1
            return
        end if

        ! - SPsi = SPsiExt
        if (ijob == SID_RESTART + 7) then
            if (ovlp_is_unit) then
                call rci_op_null(task)
            else
                call rci_op_copy(iS, task, 'N', MID_SPsiExt, MID_SPsi)
            end if
            ijob = ijob + 1
            return
        end if


        ! -- WORK1 = HR*VRsub
        if (ijob == SID_RESTART + 8) then
            call rci_op_gemm(iS, task, 'N', 'N', n, n_state, n, 1.0_r8, &
                             MID_HR, max_n, MID_VRsub, max_n, 0.0_r8, &
                             MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- HR = VRsub'*WORK1
        if (ijob == SID_RESTART + 9) then
            call rci_op_gemm(iS, task, 'C', 'N', n_state, n_state, &
                             n, 1.0_r8, &
                             MID_VRsub, max_n, MID_WORK1, max_n, 0.0_r8, &
                             MID_HR, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- WORK1 = SR*VRsub
        if (ijob == SID_RESTART + 10) then
            call rci_op_gemm(iS, task, 'N', 'N', n, n_state, n, 1.0_r8, &
                             MID_SR, max_n, MID_VRsub, max_n, 0.0_r8, &
                             MID_WORK1, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- SR = VRsub'*WORK1
        if (ijob == SID_RESTART + 11) then
            call rci_op_gemm(iS, task, 'C', 'N', n_state, n_state, &
                             n, 1.0_r8, &
                             MID_VRsub, max_n, MID_WORK1, max_n, 0.0_r8, &
                             MID_SR, max_n)
            ijob = ijob + 1
            return
        end if

        ! -- Prepare size for restarting
        if (ijob == SID_RESTART + 12) then
            n = n_state
            next = min(n_restart,max_n-n_restart)
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! EVsub = EV(not Vec_conv)
        if (ijob == SID_RESTART + 13) then
            iti = 0
            do it = 1, n
                if (Vec_conv(it) < 0.5_r8) then
                    iti = iti + 1
                    Vec_evsub(iti) = Vec_ev(it)
                end if
            end do
            call rci_op_null(task)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = SPsi(:,not conv)
        if (ijob == SID_RESTART + 14) then
            if (ovlp_is_unit) then
                call rci_op_subcol(iS, task, m, n_state, MID_Psi, MID_SPsiExt)
            else
                call rci_op_subcol(iS, task, m, n_state, MID_SPsi, MID_SPsiExt)
            end if
            ijob = ijob + 1
            return
        end if

        ! - PsiExt = HPsi(:,not conv)
        if (ijob == SID_RESTART + 15) then
            call rci_op_subcol(iS, task, m, n_state, MID_HPsi, MID_PsiExt)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = SPsiExt * Diag(EWsub)
        if (ijob == SID_RESTART + 16) then
            resvec = Vec_evsub
            call rci_op_colscale(iS, task, m, next, MID_SPsiExt, m)
            ijob = ijob + 1
            return
        end if

        ! - SPsiExt = HPsi - SPsi*Diag(EWsub)
        if (ijob == SID_RESTART + 17) then
            call rci_op_axpy(iS, task, m, next, -1.0_r8, &
                             MID_PsiExt, m, MID_SPsiExt, m)
            ijob = SID_PROJ
            return
        end if

        ! - Finish the iteration
        if (ijob == SID_FINISH) then
            resvec = Vec_conv
            iti = next
            do it = 1, n
                if (Vec_conv(it) < 0.5_r8) then
                    resvec(it) = 1.0_r8
                    next = next - 1
                end if
                if (next == 0) then
                    exit
                end if
            end do
            call rci_op_subcol(iS, task, max_n, n_state, MID_VR, MID_VRsub)
            ijob = ijob + 1
            return
        end if

        ! - PsiExt = Psi * VR
        if (ijob == SID_FINISH + 1) then
            call rci_op_gemm(iS, task, 'N', 'N', m, n_state, n, 1.0_r8, &
                             MID_Psi, m, MID_VRsub, max_n, 0.0_r8, &
                             MID_PsiExt, m)
            ijob = ijob + 1
            return
        end if

        ! - Psi = PsiExt
        if (ijob == SID_FINISH + 2) then
            call rci_op_copy(iS, task, 'N', MID_PsiExt, MID_Psi)
            ijob = ijob + 1
            return
        end if

        ! - Converge or Stop
        if (ijob == SID_FINISH + 3) then
            r_h%total_energy = sum(Vec_ev(1:n_state))
            r_h%total_iter = niter
            if (r_h%verbose > 0) then
                write(*,*) 'Eigenvalues are:'
                print *, Vec_ev(1:n_state)
            end if
            deallocate (Vec_conv)
            deallocate (Vec_ev)
            deallocate (Vec_evsub)
            deallocate (Vec_evold)
            call rci_op_converge(task, next == 0)
            return
        end if

    end subroutine

end module ELSI_RCI_DAVIDSON
