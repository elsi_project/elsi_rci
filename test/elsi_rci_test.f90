! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This program tests ELSI.
!!
program elsi_rci_test

   use ELSI_RCI_PRECISION, only: i4

   implicit none

   character(128) :: arg1 ! ev or dm
   character(128) :: arg2 ! dense or sparse
   character(128) :: arg3 ! real or cmplx
   character(128) :: arg4 ! solver
   character(128) :: arg5 ! H file
   character(128) :: arg6 ! S file

   integer(i4) :: solver
   integer(i4) :: mat_type

   ! Read command line arguments
   if(command_argument_count() >= 5) then
      call get_command_argument(1,arg1)
      call get_command_argument(2,arg2)
      call get_command_argument(3,arg3)
      call get_command_argument(4,arg4)
      call get_command_argument(5,arg5)
      read(arg2,*) mat_type
      if (mat_type == 3) then
         if(command_argument_count() == 6) then
            call get_command_argument(6,arg6)
        elseif(command_argument_count() > 6) then
            call test_die()
         endif
      else
         if(command_argument_count() == 6) then
            call get_command_argument(6,arg6)
         else
            call test_die()
         endif
      endif
      read(arg4,*) solver
   else
      call test_die()
   endif

   select case(arg1(1:1))
   case("e") ! ev
      select case(arg2(1:1))
      case("0") ! BLACS_DENSE
         select case(arg3(1:1))
         case("r") ! real
            call test_rci_ev_real_den(solver,arg5,arg6)
         case("c") ! complex
            call test_rci_ev_cmplx_den(solver,arg5,arg6)
         case default
            call test_die()
         end select
      case("1") ! PEXSI_CSC
         select case(arg3(1:1))
         case("r") ! real
!            call test_rci_ev_real_csc(solver,arg5,arg6)
         case("c") ! complex
!            call test_rci_ev_cmplx_csc(solver,arg5,arg6)
            call test_die()
         case default
            call test_die()
         end select
      case("2") ! SIESTA_CSC
         select case(arg3(1:1))
         case("r") ! real
!            call test_rci_ev_real_csc(solver,arg5,arg6)
         case("c") ! complex
            call test_die()
         case default
            call test_die()
         end select
      case("3") ! Planewave
         select case(arg3(1:1))
         case("c") ! complex
            call test_rci_ev_cmplx_pw_unit(solver,arg5)
         case default
            call test_die()
         end select
      case default
         call test_die()
      end select
   case("d") ! dm
      call test_die()
   case default
      call test_die()
   end select

contains

subroutine test_die()

   implicit none

   write(*,"(A)") "  ########################################"
   write(*,"(A)") "  ##                                    ##"
   write(*,"(A)") "  ##  Wrong command line argument(s)!!  ##"
   write(*,"(A)") "  ##                                    ##"
   write(*,"(A)") "  ##  Arg #1: 'ev' or 'dm'              ##"
   write(*,"(A)") "  ##  Arg #2: 0 = BLACS_DENSE           ##"
   write(*,"(A)") "  ##          1 = PEXSI_CSC             ##"
   write(*,"(A)") "  ##          2 = SIESTA_CSC            ##"
   write(*,"(A)") "  ##          3 = KSSOLV_PW             ##"
   write(*,"(A)") "  ##  Arg #3: 'real' or 'complex'       ##"
   write(*,"(A)") "  ##  Arg #4: 1 = RCI_Davidson          ##"
   write(*,"(A)") "  ##          2 = RCI_OMM               ##"
   write(*,"(A)") "  ##          3 = RCI_PPCG              ##"
   write(*,"(A)") "  ##          4 = RCI_ChebFilter        ##"
   write(*,"(A)") "  ##  Arg #5: H matrix file             ##"
   write(*,"(A)") "  ##  Arg #6: S matrix file             ##"
   write(*,"(A)") "  ##                                    ##"
   write(*,"(A)") "  ########################################"
   stop

end subroutine

end program
