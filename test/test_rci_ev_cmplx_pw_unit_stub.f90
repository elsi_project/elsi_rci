! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This subroutine tests complex iterative eigensolver, BLACS_DENSE format.
!!
subroutine test_rci_ev_cmplx_pw_unit(solver,h_file)

    use ELSI_RCI ! User should use this
    use ELSI_RCI_PRECISION ! Only for test
    use ELSI_RCI_READ_MAT ! Only for test
    use ELSI_RCI_WRITE_MAT ! Only for test
    use ELSI_RCI_TIMER ! Only for test

    implicit none

    integer(i4), intent(in) :: solver
    character(*), intent(in) :: h_file

    write (*, '("  pw tests not activated due to lack of MKL_FFTI ")')
    write (*, '("  Passed.")')

end subroutine
