! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to read matrices.
!!
module ELSI_RCI_READ_MAT

   use ELSI_RCI_PRECISION, only: r8,i4,i8

   implicit none

   private

   public :: rci_read
   public :: rci_read_pw
   public :: rci_read_clean

   integer(i4), public :: n_basis
   integer(i4), public :: n_basis_1D(3)
   integer(i4), public :: n_basis_pw
   integer(i4), public :: n_electron
   integer(i4), public :: n_h_vnl

   real(r8),    allocatable, public :: h_real(:,:)
   real(r8),    allocatable, public :: s_real(:,:)
   complex(r8), allocatable, public :: h_cmplx(:,:)
   complex(r8), allocatable, public :: s_cmplx(:,:)

   integer(i4), allocatable, public :: h_idx(:)
   complex(r8), allocatable, public :: h_gkin(:)
   complex(r8), allocatable, public :: h_vtot(:)
   complex(r8), allocatable, public :: h_vnlmat(:,:)
   complex(r8), allocatable, public :: h_vnlsign(:)

contains

subroutine rci_read(h_file,s_file,is_real)

   implicit none

   character(*), intent(in) :: h_file
   character(*), intent(in) :: s_file
   logical,      intent(in) :: is_real

   integer(i4) :: i_row
   integer(i4) :: i_col
   integer(i4) :: header(16)
   integer(i4) :: i_val
   integer(i4) :: nnz
   integer(i8) :: offset

   real(r8),    allocatable :: h_val_real(:)
   real(r8),    allocatable :: s_val_real(:)
   complex(r8), allocatable :: h_val_cmplx(:)
   complex(r8), allocatable :: s_val_cmplx(:)
   integer(i4), allocatable :: row_ind(:)
   integer(i4), allocatable :: col_ptr(:)

   ! Open file
   open(file=h_file,unit=30,access="stream",form="unformatted")

   ! Read header
   offset = 1

   read(unit=30,pos=offset) header

   n_basis    = header(4)
   n_electron = header(5)
   nnz        = header(6)

   if(is_real) then
      allocate(h_val_real(nnz))
   else
      allocate(h_val_cmplx(nnz))
   endif
   allocate(row_ind(nnz))
   allocate(col_ptr(n_basis+1))

   ! Read column pointer
   offset = offset+64

   read(unit=30,pos=offset) col_ptr(1:n_basis)

   col_ptr(n_basis+1) = nnz+1

   offset = offset+n_basis*4

   ! Read row index
   read(unit=30,pos=offset) row_ind

   offset = offset+nnz*4

   ! Read non-zero value
   if(is_real) then
      read(unit=30,pos=offset) h_val_real
   else
      read(unit=30,pos=offset) h_val_cmplx
   endif

   ! Close file
   close(unit=30)

   if(is_real) then
      allocate(h_real(n_basis,n_basis))
   else
      allocate(h_cmplx(n_basis,n_basis))
   endif

   ! Convert to dense and symmetrize
   if(is_real) then
      i_col  = 0
      h_real = 0.0_r8

      do i_val = 1,nnz
         if(i_val == col_ptr(i_col+1) .and. i_col /= n_basis) then
            i_col = i_col+1
         endif

         i_row               = row_ind(i_val)
         h_real(i_row,i_col) = h_val_real(i_val)
         h_real(i_col,i_row) = h_val_real(i_val)
      enddo

      deallocate(h_val_real)
      deallocate(row_ind)
      deallocate(col_ptr)
   else
      i_col   = 0
      h_cmplx = (0.0_r8,0.0_r8)

      do i_val = 1,nnz
         if(i_val == col_ptr(i_col+1) .and. i_col /= n_basis) then
            i_col = i_col+1
         endif

         i_row                = row_ind(i_val)
         h_cmplx(i_row,i_col) = h_val_cmplx(i_val)
         h_cmplx(i_col,i_row) = h_val_cmplx(i_val)
      enddo

      deallocate(h_val_cmplx)
      deallocate(row_ind)
      deallocate(col_ptr)
   endif

   ! Open file
   open(file=s_file,unit=30,access="stream",form="unformatted")

   ! Read header
   offset = 1

   read(unit=30,pos=offset) header

   n_basis    = header(4)
   n_electron = header(5)
   nnz        = header(6)

   if(is_real) then
      allocate(s_val_real(nnz))
   else
      allocate(s_val_cmplx(nnz))
   endif
   allocate(row_ind(nnz))
   allocate(col_ptr(n_basis+1))

   ! Read column pointer
   offset = offset+64

   read(unit=30,pos=offset) col_ptr(1:n_basis)

   col_ptr(n_basis+1) = nnz+1

   offset = offset+n_basis*4

   ! Read row index
   read(unit=30,pos=offset) row_ind

   offset = offset+nnz*4

   ! Read non-zero value
   if(is_real) then
      read(unit=30,pos=offset) s_val_real
   else
      read(unit=30,pos=offset) s_val_cmplx
   endif ! Close file
   close(unit=30)

   if(is_real) then
      allocate(s_real(n_basis,n_basis))
   else
      allocate(s_cmplx(n_basis,n_basis))
   endif

   ! Convert to dense and symmetrize
   if(is_real) then
      i_col  = 0
      s_real = 0.0_r8

      do i_val = 1,nnz
         if(i_val == col_ptr(i_col+1) .and. i_col /= n_basis) then
            i_col = i_col+1
         endif

         i_row               = row_ind(i_val)
         s_real(i_row,i_col) = s_val_real(i_val)
         s_real(i_col,i_row) = s_val_real(i_val)
      enddo

      deallocate(s_val_real)
      deallocate(row_ind)
      deallocate(col_ptr)
   else
      i_col   = 0
      s_cmplx = (0.0_r8,0.0_r8)

      do i_val = 1,nnz
         if(i_val == col_ptr(i_col+1) .and. i_col /= n_basis) then
            i_col = i_col+1
         endif

         i_row                = row_ind(i_val)
         s_cmplx(i_row,i_col) = s_val_cmplx(i_val)
         s_cmplx(i_col,i_row) = s_val_cmplx(i_val)
      enddo

      deallocate(s_val_cmplx)
      deallocate(row_ind)
      deallocate(col_ptr)
   endif

end subroutine

subroutine rci_read_pw(h_file,is_real)

   implicit none

   character(*), intent(in) :: h_file
   logical,      intent(in) :: is_real

   integer(i4) :: header(7)
   integer(i4) :: i_val
   integer(i4) :: nnz
   integer(i8) :: offset

   ! Open file
   open(file=h_file,unit=30,access="sequential",form="formatted")

   ! Read header
   read(unit=30, fmt=*) header

   n_basis       = header(1)
   n_basis_1D(1) = header(2)
   n_basis_1D(2) = header(3)
   n_basis_1D(3) = header(4)
   n_basis_pw    = header(5)
   n_electron    = header(6)
   n_h_vnl       = header(7)

   allocate(h_idx(n_basis_pw))
   allocate(h_gkin(n_basis_pw))
   allocate(h_vtot(n_basis))
   allocate(h_vnlmat(n_basis_pw,n_h_vnl))
   allocate(h_vnlsign(n_h_vnl))

   read(unit=30, fmt=*) h_idx

   read(unit=30, fmt=*) h_gkin

   read(unit=30, fmt=*) h_vtot

   read(unit=30, fmt=*) h_vnlmat

   read(unit=30, fmt=*) h_vnlsign

   ! Close file
   close(unit=30)

end subroutine

subroutine rci_read_clean()

   implicit none

   if(allocated(h_real)) then
      deallocate(h_real)
   endif

   if(allocated(s_real)) then
      deallocate(s_real)
   endif

   if(allocated(h_cmplx)) then
      deallocate(h_cmplx)
   endif

   if(allocated(s_cmplx)) then
      deallocate(s_cmplx)
   endif

   if(allocated(h_idx)) then
      deallocate(h_idx)
   endif

   if(allocated(h_gkin)) then
      deallocate(h_gkin)
   endif

   if(allocated(h_vtot)) then
      deallocate(h_vtot)
   endif

   if(allocated(h_vnlmat)) then
      deallocate(h_vnlmat)
   endif

   if(allocated(h_vnlsign)) then
      deallocate(h_vnlsign)
   endif

end subroutine

end module ELSI_RCI_READ_MAT
