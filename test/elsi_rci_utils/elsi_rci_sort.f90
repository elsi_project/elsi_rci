! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to sort an array.
!!
module ELSI_RCI_SORT

    use ELSI_RCI_PRECISION, only: r8,i4

    implicit none

    public :: rci_sort_bubble

    private

contains

subroutine rci_sort_bubble(ev,n,V,compar)

    implicit none

    real(r8), intent(inout) :: ev(:)
    integer(i4), intent(in) :: n
    real(r8), intent(inout) :: V(:,:)

    interface
        integer(i4) function compar(a,b)
            use ELSI_RCI_PRECISION, only: r8,i4
            real(r8), intent(in) :: a, b
        end function compar
    end interface

    real(r8) :: tmpev
    real(r8), allocatable :: tmpV(:)

    integer(i4) :: itsup, itbub

    allocate(tmpV(size(V,1)))

    do itsup = n-1,1,-1
        do itbub = itsup,n-1,1
            if ( compar(ev(itbub),ev(itbub+1)) > 0 ) then
                tmpev = ev(itbub)
                ev(itbub) = ev(itbub+1)
                ev(itbub+1) = tmpev
                tmpV = V(:,itbub)
                V(:,itbub) = V(:,itbub+1)
                V(:,itbub+1) = tmpV
            else
                exit
            endif
        enddo
    enddo

    deallocate(tmpV)

end subroutine

end module ELSI_RCI_SORT
